# Curso: Database Programming with SQL


[1. Crear tablas (create table - describe - all_tables - drop table)](manual/1.md)

[2. Ingresar registros (insert into- select)](manual/2.md)

[3. Tipos de datos](manual/3.md)

[4. Recuperar algunos campos (select)](manual/4.md)

[5. Recuperar algunos registros (where)](manual/5.md)

[6. Operadores relacionales](manual/6.md)

[7. Borrar registros (delete)](manual/7.md)

[8. Actualizar registros (update)](manual/8.md)

[9. Comentarios](manual/9.md)

[10. Valores nulos (null)](manual/10.md)

[11. Operadores relacionales (is null)](manual/11.md)

[12. Clave primaria (primary key)](manual/12.md)

[13. Vaciar la tabla (truncate table)](manual/13.md)

[14. Tipos de datos alfanuméricos](manual/14.md)

[15. Tipos de datos numéricos](manual/15.md)

[16. Ingresar algunos campos](manual/16.md)

[17. Valores por defecto (default)](manual/17.md)

[18. Operadores aritméticos y de concatenación (columnas calculadas)](manual/18.md)

[19.  Alias (encabezados de columnas)](manual/19.md)

[20. Funciones string](manual/20.md)

[21. Funciones matemáticas](manual/21.md)

[22. Funciones de fechas y horas](manual/22.md)

[23. Ordenar registros (order by)](manual/23.md)

[24. Operadores lógicos (and - or - not)](manual/24.md)

[25. Otros operadores relacionales (between)](manual/25.md)

[26. Otros operadores relacionales (in)](manual/26.md)

[27. Búsqueda de patrones (like - not like)](manual/27.md)

[28. Contar registros (count)](manual/28.md)

[29. Funciones de grupo (count - max - min - sum - avg)](manual/28.md)

[30. Agrupar registros (group by)](manual/30.md)

[31. Seleccionar grupos (Having)](manual/31.md)

[32. Registros duplicados (Distinct)](manual/32.md)

[33. Clave primaria compuesta](manual/33.md)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](manual/34.md)

[35. Alterar secuencia (alter sequence)](manual/35.md)

[36. Integridad de datos](manual/36.md)

[37. Restricción primary key](manual/37.md)

[38. Restricción unique](manual/38.md)

[39. Restriccioncheck](manual/39.md)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](manual/40.md)

[41. Restricciones: información (user_constraints - user_cons_columns)](manual/41.md)

[42. Restricciones: eliminación (alter table - drop constraint)](manual/42.md)

[43. Indices](manual/43.md)

[44. Indices (Crear . Información)](manual/44.md)

[45. Indices (eliminar)](manual/45.md)

[46. Varias tablas (join)](manual/46.md)

[47. Combinación interna (join)](manual/47.md)

[48. Combinación externa izquierda (left join)](manual/48.md)

[49. Combinación externa derecha (right join)](manual/49.md)

[50. Combinación externa completa (full join)](manual/50.md)

[51. Combinaciones cruzadas (cross)](manual/51.md)

[52. Autocombinación](manual/52.md)

[53. Combinaciones y funciones de agrupamiento	](manual/53.md)

[54. Combinar más de 2 tablas](manual/54.md)

## Modelo de presentacion

[repositorio](https://gitlab.com/senati7912512/base-datos)

## Libros referenciales

[Oracle 12c FORMS y REPORTS-Curso práctico de formación](https://drive.google.com/file/d/1vlQioFvdtYCoPY6jY0Tq_UTaro3wMjcm/edit)

[Sistemas Gestores de Bases de Datos](https://drive.google.com/file/d/1VYaSpy-uT1lghwK9hPjAr6U4BBsbBQ9D/edit)

[Administración básica de base de datos con Oracle 12c SQL](https://drive.google.com/file/d/1HGcdUc_SgbaovtvEP1uBV9ZTUbSq_lPJ/edit)

## Backup's

[HR Data Base](https://drive.google.com/file/d/1uOe4LY18W32i5yWcazwf1k-NNXMmTIXs/edit)

## Presentaciones del curso de Oracle Academy

### 1. Introducción

[1.1. Oracle Application Express](https://drive.google.com/file/d/1hjve3XEb0mpr6KzFhkFet4muM4upDkpR/edit)

[1.2. Tecnología de Base de Datos Relacional](https://drive.google.com/file/d/1Mwc5rv6ZZ96SYQaf4ixqeeaFjPrYlfsL/edit)

[1.3. Anatomía de una Sentencia SQL](https://drive.google.com/file/d/1cLFU8jCl1JDJ1h4GRJIFaIXjh4jciJYt/edit)

[1.4. Anatomía de una Sentencia SQL](https://drive.google.com/file/d/1cLFU8jCl1JDJ1h4GRJIFaIXjh4jciJYt/edit)

## Presentaciones adicionales

[1. Introducción](https://docs.google.com/presentation/d/19Fq-uNNwYiuTIOtJxIlBD7a9uOQrmwv9/edit)
