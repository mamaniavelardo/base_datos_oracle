# 20. Funciones string

Las funciones de manejo de caracteres alfanuméricos aceptan argumentos de tipo caracter y retornan caracteres o valores numéricos.

Las siguientes son algunas de las funciones que ofrece Oracle para trabajar con cadenas de caracteres:

- chr(x): retorna un caracter equivalente al código enviado como argumento "x". Ejemplo:

```sql
select chr(65) from dual;-- retorna 'A'.
select chr(100) from dual;-- retorna 'd'.
```

Acotaciones

La tabla DUAL es una tabla especial de una sola columna presente de manera predeterminada en todas las instalaciones de bases de datos de Oracle. Se utiliza cuando queremos hacer unselect que no necesita consultar tablas. La tabla tiene una sola columna VARCHAR2(1) llamada DUMMY que tiene un valor de 'X'

- concat(cadena1,cadena2): concatena dos cadenas de caracteres; es equivalente al operador ||. Ejemplo:

```sql
select concat('Buenas',' tardes') from dual;--retorna 'Buenas tardes'.
```

- initcap(cadena): retorna la cadena enviada como argumento con la primera letra (letra capital) de cada palabra en mayúscula. Ejemplo:

```sql
select initcap('buenas tardes alumno') from dual;--retorna 'Buenas Tardes Alumno'.
```

- lower(cadena): retorna la cadena enviada como argumento en minúsculas. "lower" significa reducir en inglés. Ejemplo:

```sql
select lower('Buenas tardes ALUMNO') from dual;--retorna "buenas tardes alumno".
```

- upper(cadena): retorna la cadena con todos los caracteres en mayúsculas. Ejemplo:

```sql
select upper('www.oracle.com') from dual;-- 'WWW.ORACLE.COM'
```

- lpad(cadena,longitud,cadenarelleno): retorna la cantidad de caracteres especificados por el argumento "longitud", de la cadena enviada como primer argumento (comenzando desde el primer caracter); si "longitud" es mayor que el tamaño de la cadena enviada, rellena los espacios restantes con la cadena enviada como tercer argumento (en caso de omitir el tercer argumento rellena con espacios); el relleno comienza desde la izquierda. Ejemplos:

```sql
select lpad('alumno',10,'xyz') from dual;-- retorna 'xyzxalumno'
select lpad('alumno',4,'xyz') from dual;-- retorna 'alum'
```

- rpad(cadena,longitud,cadenarelleno): retorna la cantidad de caracteres especificados por el argumento "longitud", de la cadena enviada como primer argumento (comenzando desde el primer caracter); si "longitud" es mayor que el tamaño de la cadena enviada, rellena los espacios restantes con la cadena enviada como tercer argumento (en caso de omitir el tercer argumento rellena con espacios); el relleno comienza desde la derecha (último caracter). Ejemplos:

```sql
select rpad('alumno',10,'xyz') from dual;-- retorna 'alumnoxyzx'
select rpad('alumno',4,'xyz') from dual;-- retorna 'alum'
```

- ltrim(cadena1,cadena2): borra todas las ocurrencias de "cadena2" en "cadena1", si se encuentran al comienzo; si se omite el segundo argumento, se eliminan los espacios. Ejemplo:

```sql
select ltrim('la casa de la cuadra','la') from dual;-- ' casa de la cuadra'
select ltrim(' es la casa de la cuadra','la') from dual;-- no elimina ningún caracter
select ltrim('  la casa') from dual;-- 'la casa'
```

- rtrim(cadena1,cadena2): borra todas las ocurrencias de "cadena2" en "cadena1", si se encuentran por la derecha (al final de la cadena); si se omite el segundo argumento, se borran los espacios. Ejemplo:

```sql
select rtrim('la casa lila','la') from dual;-- 'la casa li'
select rtrim('la casa lila ','la') from dual;-- no borra ningún caracter
select rtrim('la casa lila    ') from dual; --'la casa lila'
```

- trim(cadena): retorna la cadena con los espacios de la izquierda y derecha eliminados. "Trim" significa recortar. Ejemplo:

```sql
select trim('   oracle     ') from dual;--'oracle'
```

- replace(cadena,subcade1,subcade2): retorna la cadena con todas las ocurrencias de la subcadena de reemplazo (subcade2) por la subcadena a reemplazar (subcae1). Ejemplo:

```sql
select replace('xxx.oracle.com','x','w') from dual;
```

retorna "www.oracle.com'.

- substr(cadena,inicio,longitud): devuelve una parte de la cadena especificada como primer argumento, empezando desde la posición especificada por el segundo argumento y de tantos caracteres de longitud como indica el tercer argumento. Ejemplo:

```sql
select substr('www.oracle.com',1,10) from dual;-- 'www.oracle'
select substr('www.oracle.com',5,6) from dual;-- 'oracle'
```

- length(cadena): retorna la longitud de la cadena enviada como argumento. "lenght" significa longitud en inglés. Ejemplo:

```sql
select length('www.oracle.com') from dual;-- devuelve 14.
```

- instr (cadena,subcadena): devuelve la posición de comienzo (de la primera ocurrencia) de la subcadena especificada en la cadena enviada como primer argumento. Si no la encuentra retorna 0. Ejemplos:

```sql
select instr('Jorge Luis Borges','or') from dual;-- 2
select instr('Jorge Luis Borges','ar') from dual;-- 0, no se encuentra
```

- translate(): reemplaza cada ocurrencia de una serie de caracteres con otra serie de caracteres. La diferencia con "replace" es que aquella trabaja con cadenas de caracteres y reemplaza una cadena completa por otra, en cambio "translate" trabaja con caracteres simples y reemplaza varios. En el siguiente ejemplo se especifica que se reemplacen todos los caracteres "O" por el caracter "0", todos los caracteres "S" por el caracter "5" y todos los caracteres "G" por "6":

```sql
select translate('JORGE LUIS BORGES','OSG','056') from dual;--'J0R6E LUI5 B0R6E5'
```

Se pueden emplear estas funciones enviando como argumento el nombre de un campo de tipo caracter.

## Ejercicios de laboratorio

Trabajamos con la tabla "libros" de una librería.

Eliminamos la tabla:

```sql
drop table libros;
```

Creamos la tabla:

```sql
create table libros(
    codigo number(5),
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    precio number(6,2),
    cantidad number(3)
);
```

Ingresamos algunos registros:

```sql
insert into libros
values(1,'El aleph','Borges','Emece',25,100);

insert into libros
values(2,'Java en 10 minutos','Mario Molina','Siglo XXI',50.40,100);

insert into libros
values(3,'Alicia en el pais de las maravillas','Lewis Carroll','Emece',15.50,200);

insert into libros
values(4,'El pais de las hadas',default,'Emece',25.50,150);
```

Mostramos sólo los 12 primeros caracteres de los títulos de los libros y sus autores, empleando la función "substr":

```sql
select substr(titulo,1,12) as titulo
from libros;
```

Mostramos sólo los 20 primeros caracteres de los títulos de los libros y rellenando los espacios restantes con "*", empleando la función "rpad":

```sql
select rpad(titulo,20,'*') as titulo
from libros;
```

Mostramos los títulos de los libros empleando la función "initcap":

```sql
select initcap(titulo) as titulo
from libros;
```

Note que cada palabra comienza con mayúsculas.

Mostramos los títulos de los libros y sus autores en mayúsculas:

```sql
select titulo,upper(autor) as autor
from libros;
```

Concatenamos título y autor empleando "concat":

```sql
select concat(titulo, autor)
from libros;
```

Mostramos el título y el precio de todos los libros concatenando el signo "$" a los precios:

```sql
select titulo,concat('$ ',precio) as precio
from libros;
```

Recuperamos el título y editorial de "libros" reemplazando "Emece" por "Sudamericana":

```sql
select titulo,replace(editorial,'Emece','Sudamericana')
from libros;
```

Recuperamos el autor de todos los libros reemplazando las letras "abc" por "ABC" respectivamente (empleando "translate"):

```sql
select translate(autor,'abc','ABC') from libros;
```

Note que cada caracter individual es reemplazado por el especificado.

Mostramos la posición de la primera ocurrencia de la cadena "pais" en los títulos de los libros:

```sql
select instr(titulo,'pais') from libros;
```

Note que los títulos que no contienen la subcadena "pais" muestran el valor cero.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    codigo number(5),
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    precio number(6,2),
    cantidad number(3)
);

insert into libros
 values(1,'El aleph','Borges','Emece',25,100);

insert into libros
values(2,'Java en 10 minutos','Mario Molina','Siglo XXI',50.40,100);

insert into libros
values(3,'Alicia en el pais de las maravillas','Lewis Carroll','Emece',15.50,200);

insert into libros
values(4,'El pais de las hadas',default,'Emece',25.50,150);

select substr(titulo,1,12) as titulo
from libros;

select rpad(titulo,20,'*') as titulo
from libros;

select initcap(titulo) as titulo
from libros;

select titulo,upper(autor) as autor
from libros;

select concat(titulo, autor)
from libros;

select titulo,concat('$ ',precio) as precio
from libros;

select titulo,replace(editorial,'Emece','Sudamericana')
from libros;

select translate(autor,'abc','ABC') from libros;

select instr(titulo,'pais') from libros;
```
