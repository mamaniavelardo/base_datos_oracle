# 10. Valores nulos (null)

"null' significa "dato desconocido" o "valor inexistente".

A veces, puede desconocerse o no existir el dato correspondiente a algún campo de un registro. En estos casos decimos que el campo puede contener valores nulos.

Por ejemplo, en nuestra tabla de libros, podemos tener valores nulos en el campo "precio" porque es posible que para algunos libros no le hayamos establecido el precio para la venta.

En contraposición, tenemos campos que no pueden estar vacíos jamás.

Veamos un ejemplo. Tenemos nuestra tabla "libros". El campo "titulo" no debería estar vacío nunca, igualmente el campo "autor". Para ello, al crear la tabla, debemos especificar que tales campos no admitan valores nulos:

```sql
create table libros(
    titulo varchar2(30) not null,
    autor varchar2(20) not null,
    editorial varchar2(15) null,
    precio number(5,2) 
);
```

Para especificar que un campo NO admita valores nulos, debemos colocar "not null" luego de la definición del campo.

En el ejemplo anterior, los campos "editorial" y "precio" si admiten valores nulos.

Cuando colocamos "null" estamos diciendo que admite valores nulos (caso del campo "editorial"); por defecto, es decir, si no lo aclaramos, los campos permiten valores nulos (caso del campo "precio").

Cualquier campo, de cualquier tipo de dato permite ser definido para aceptar o no valores nulos. Un valor "null" NO es lo mismo que un valor 0 (cero) o una cadena de espacios en blanco (" ").

Si ingresamos los datos de un libro, para el cual aún no hemos definido el precio podemos colocar "null" para mostrar que no tiene precio:

```sql
insert into libros (titulo,autor,editorial,precio) values('El aleph','Borges','Emece',null);
```

Note que el valor "null" no es una cadena de caracteres, NO se coloca entre comillas.

Entonces, si un campo acepta valores nulos, podemos ingresar "null" cuando no conocemos el valor.

También podemos colocar "null" en el campo "editorial" si desconocemos el nombre de la editorial a la cual pertenece el libro que vamos a ingresar:

```sql
insert into libros (titulo,autor,editorial,precio) values('Alicia en el pais','Lewis Carroll',null,25);
```

Una cadena vacía es interpretada por Oracle como valor nulo; por lo tanto, si ingresamos una cadena vacía, se almacena el valor "null".

Si intentamos ingresar el valor "null" (o una cadena vacía) en campos que no admiten valores nulos (como "titulo" o "autor"), Oracle no lo permite, muestra un mensaje y la inserción no se realiza; por ejemplo:

```sql
insert into libros (titulo,autor,editorial,precio) values(null,'Borges','Siglo XXI',25);
```

Cuando vemos la estructura de una tabla con "describe", en la columna "Null", aparece "NOT NULL" si el campo no admite valores nulos y no aparece en caso que si los permita.

Para recuperar los registros que contengan el valor "null" en algún campo, no podemos utilizar los operadores relacionales vistos anteriormente: = (igual) y <> (distinto); debemos utilizar los operadores "is null" (es igual a null) y "is not null" (no es null).

Los valores nulos no se muestran, aparece el campo vacío.

Entonces, para que un campo no permita valores nulos debemos especificarlo luego de definir el campo, agregando "not null". Por defecto, los campos permiten valores nulos, pero podemos especificarlo igualmente agregando "null".

# Ejercicios de laboratorio

Trabajamos con la tabla "libros" de una librería.

Eliminamos la tabla "libros":

```sql
drop table libros;
```

Creamos la tabla especificando que los campos "titulo" y "autor" no admitan valores nulos:

```sql
create table libros(
    titulo varchar2(30) not null,
    autor varchar2(30) not null,
    editorial varchar2(15) null,
    precio number(5,2)
);
```

Los campos "editorial" y "precio" si permiten valores nulos; el primero, porque lo especificamos colocando "null" en la definición del campo, el segundo lo asume por defecto.

Agregamos un registro a la tabla con valor nulo para el campo "precio":

```sql
insert into libros (titulo,autor,editorial,precio) values('El aleph','Borges','Emece',null);
```

Veamos cómo se almacenó el registro:

```sql
select *from libros;
```

No aparece ningún valor en la columna "precio".

Ingresamos otro registro, con valor nulo para el campo "editorial", campo que admite valores "null":

```sql
insert into libros (titulo,autor,editorial,precio) values('Alicia en el pais','Lewis Carroll',null,0);
```

Veamos cómo se almacenó el registro:

```sql
select *from libros;
```

No aparece ningún valor en la columna "editorial".

Ingresamos otro registro, con valor nulo para los dos campos que lo admiten:

```sql
insert into libros (titulo,autor,editorial,precio) values('Aprenda PHP','Mario Molina',null,null);
```

Veamos cómo se almacenó el registro:

```sql
select *from libros;
```

No aparece ningún valor en ambas columnas.

Veamos lo que sucede si intentamos ingresar el valor "null" en campos que no lo admiten, como "titulo":

```sql
insert into libros (titulo,autor,editorial,precio) values(null,'Borges','Siglo XXI',25);
```

Aparece un mensaje indicando que no se puede realizar una inserción "null" y la sentencia no se ejecuta.

Para ver cuáles campos admiten valores nulos y cuáles no, vemos la estructura de la tabla:

```sql
describe libros;
```

Nos muestra, en la columna "Null", que los campos "titulo" y "autor" están definidos "not null", es decir, no permiten valores nulos, los otros dos campos si los admiten.

Dijimos que la cadena vacía es interpretada como valor "null". Vamos a ingresar un registro con cadena vacía para el campo "editorial":

```sql
insert into libros (titulo,autor,editorial,precio) values('Uno','Richard Bach','',18.50);
```

Veamos cómo se almacenó el registro:

```sql
select *from libros;
```

No aparece ningún valor en la columna "editorial" del libro "Uno", almacenó "null".

Intentamos ingresar una cadena vacía en el campo "titulo":

```sql
insert into libros (titulo,autor,editorial,precio) values('','Richard Bach','Planeta',22);
```

Mensaje de error indicando que el campo no admite valores nulos.

Dijimos que una cadena de espacios NO es igual a una cadena vacía o valor "null". Vamos a ingresar un registro y en el campo "editorial" guardaremos una cadena de 3 espacios:

```sql
insert into libros (titulo,autor,editorial,precio)mvalues('Don quijote','Cervantes','   ',20);
```

Veamos cómo se almacenó el registro:

```sql
select *from libros;
```

Se muestra la cadena de espacios.

Recuperamos los registros que contengan en el campo "editorial" una cadena de 3 espacios:

```sql
select *from libros where editorial='   ';
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    titulo varchar2(30) not null,
    autor varchar2(30) not null,
    editorial varchar2(15) null,
    precio number(5,2)
);

insert into libros (titulo,autor,editorial,precio) values('El aleph','Borges','Emece',null);

select *from libros;

insert into libros (titulo,autor,editorial,precio) values('Alicia en el pais','Lewis Carroll',null,0);

select *from libros;

insert into libros (titulo,autor,editorial,precio) values('Aprenda PHP','Mario Molina',null,null);

select *from libros;

insert into libros (titulo,autor,editorial,precio) values(null,'Borges','Siglo XXI',25);

describe libros;

insert into libros (titulo,autor,editorial,precio) values('Uno','Richard Bach','',18.50);

select *from libros;

insert into libros (titulo,autor,editorial,precio) values('','Richard Bach','Planeta',22);

insert into libros (titulo,autor,editorial,precio) values('Don quijote','Cervantes','   ',20);

select *from libros;

select *from libros where editorial='   ';
```

# Ejercicios propuestos

## Ejercicio 01

Una farmacia guarda información referente a sus medicamentos en una tabla llamada "medicamentos".

1. Elimine la tabla y créela con la siguiente estructura:

```sql
 drop table medicamentos;
 create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);
```

2. Visualice la estructura de la tabla "medicamentos"
note que los campos "codigo", "nombre" y "cantidad", en la columna "Null" muestra "NOT NULL".

3. Ingrese algunos registros con valores "null" para los campos que lo admitan:

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100); 
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);
```

4. Vea todos los registros.

5. Ingrese un registro con valor "0" para el precio y cadena vacía para el laboratorio.

6. Intente ingresar un registro con cadena vacía para el nombre (mensaje de error)

7. Intente ingresar un registro con valor nulo para un campo que no lo admite (aparece un mensaje de error)

8. Ingrese un registro con una cadena de 1 espacio para el laboratorio.

9. Recupere los registros cuyo laboratorio contenga 1 espacio (1 registro)

10. Recupere los registros cuyo laboratorio sea distinto de ' '(cadena de 1 espacio) (1 registro)

## Ejercicio 02

Trabaje con la tabla que almacena los datos sobre películas, llamada "peliculas".

1. Elimine la tabla:

2. Créela con la siguiente estructura:

```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);
```

3. Visualice la estructura de la tabla.

note que el campo "codigo" y "titulo", en la columna "Null" muestran "NOT NULL".

4. Ingrese los siguientes registros:

```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);
```

5. Recupere todos los registros para ver cómo Oracle los almacenó.

6. Intente ingresar un registro con valor nulo para campos que no lo admiten (aparece un mensaje de error)

7. Muestre todos los registros.

8. Actualice la película en cuyo campo "duracion" hay 0 por "null" (1 registro)

9. Recupere todos los registros.

